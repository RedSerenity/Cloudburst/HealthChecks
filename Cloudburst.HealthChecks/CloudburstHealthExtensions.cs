﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Cloudburst.HealthChecks {
	public static class CloudburstHealthExtensions {
		public static IHostBuilder UseCloudburstHealthCheck(this IHostBuilder builder) {
			builder.ConfigureServices((builderContext, services) => {
				services.AddCloudburstHealthCheck();
			});

			return builder;
		}

		public static IServiceCollection AddCloudburstHealthCheck(this IServiceCollection services) {
			services.AddHealthChecks()
				.AddCheck<ServiceRunningHealthCheck>(nameof(ServiceRunningHealthCheck));

			return services;
		}

		public static IEndpointRouteBuilder MapCloudburstHealthChecks(this IEndpointRouteBuilder routes, Action<HealthCheckOptions> action = null) {
			var healthCheckOptions = new HealthCheckOptions {
				ResponseWriter = ResponseWriter.WriteResponse
			};

			action?.Invoke(healthCheckOptions);

			routes.MapHealthChecks("/health", healthCheckOptions);

			return routes;
		}
	}
}
