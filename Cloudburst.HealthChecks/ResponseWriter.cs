using System;
using System.Linq;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Cloudburst.HealthChecks.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace Cloudburst.HealthChecks {
	public class ResponseWriter {
		public static Task WriteResponse(HttpContext httpContext, HealthReport report) {
			httpContext.Response.ContentType = "application/json; charset=utf-8";
			switch (report.Status) {
				case HealthStatus.Unhealthy:
					httpContext.Response.StatusCode = StatusCodes.Status503ServiceUnavailable;
					break;
				case HealthStatus.Degraded:
					httpContext.Response.StatusCode = StatusCodes.Status424FailedDependency;
					break;
				case HealthStatus.Healthy:
					httpContext.Response.StatusCode = StatusCodes.Status200OK;
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}

			var options = new JsonSerializerOptions {
				Converters = { new JsonStringEnumConverter() },
				WriteIndented = true
			};

			var response = new HealthCheckResponse {
				Status = report.Status,
				Results = report.Entries.Select(x => new HealthCheckEntry {
					Name = x.Key,
					Status = x.Value.Status,
					Description = x.Value.Description,
					Data = x.Value.Data,
					Exception = x.Value.Exception
				})
			};

			string serializedResponse = JsonSerializer.Serialize(response, options);
			return httpContext.Response.WriteAsync(serializedResponse);
		}
	}
}
