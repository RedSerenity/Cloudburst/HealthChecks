﻿using System.Collections.Generic;
using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace Cloudburst.HealthChecks.Models {
	public class HealthCheckResponse {
		public HealthStatus Status { get; set; }
		public IEnumerable<HealthCheckEntry> Results { get; set; }
	}
}
